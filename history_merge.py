#!/usr/bin/python3

# TODO: bash script

import re
import os
import sys
import argparse

__all__ = 'Missing1Group', 'iter_history'


REGEXP = r'^(#[_0-9,.+-]+)(?:\s|$)'
INIT = '#0'

HISTORIES = (
    '~/.bash_history',
    '~/.bash_history.bak/.bash_history',
    '~/.bash_history.bak/.bash_rehistory',
    '~/.bash_history.bak/.bash_history.meta',
    '~/.bash_rehistory',
    '~/.bash_history.meta',
    )


class Missing1Group(IndexError): pass


def iter_history(lines, history=None,
            entries=None, entry=None,
            regexp=None, init=None,
            debug=None):

    ''' process binary lines, accept arguments with binary data 

        (except for debug)
    '''

    if history is None: history = {}
    if entries is None: entries = []
    if entry is None: entry = []
    if regexp is None: regexp = REGEXP.encode()
    if init is None: init = INIT.encode()

    search = re.compile(regexp).search

    if not history:
        history.setdefault(init, entries)

    register = history.setdefault
    stderr = sys.stderr
    match = None

    try:
        for line in lines:
            match = search(line)
            if match:
                key = match.group(1)
                if entry and entry not in entries:
                    if debug:
                        print('+', debug, file=stderr)
                    entries.append(entry)
                elif debug:
                    if entry:
                        print('*', debug, file=stderr)
                    else:
                        print('-', debug, file=stderr)
                entry = []
                entries = register(key, [])

            elif not entry:
                lstrip = line.lstrip()
                if not lstrip or lstrip.startswith(b'#'):
                    continue

            entry.append(line)

    except IndexError:
        if not match:
            raise
        try:
            match.group(1)
        except IndexError:
            pass
        else:
            raise
        raise Missing1Group(regexp)

    if entry and entry not in entries:
        entries.append(entry)

    return history


def main_parser(parser=None, prog=None):
    _str = lambda x: x.encode()
    if parser is None:
        parser = argparse.ArgumentParser(prog)
    parser.add_argument('-r', '--regexp', type=_str, default=_str(REGEXP), help=f'python regular expression: needs 1 group, default: {REGEXP!r}')
    parser.add_argument('-i', '--init', type=_str, default=_str(INIT), help=f'default: {INIT!r}')
    parser.add_argument('-d', '--debug', action='store_true')
    parser.add_argument('histories', nargs='*', default=HISTORIES)
    return parser


def main_process(args, history=None):
    if history is None:
        history = {}
    debug = None
    errors = []
    for path in args.histories:
        if args.debug:
            debug = path
        try:
            if path == '-':
                stream = os.fdopen(os.dup(0), 'rb')
            else:
                stream = open(os.path.expanduser(path), 'rb')
        except Exception as E:
            errors.append((path, E))
            continue
        else:
            try:
                iter_history(stream, history, regexp=args.regexp, init=args.init, debug=debug)
            finally:
                stream.close()
    return history, errors


def main_print(history):
    write = os.write
    join = b''.join
    _entry = None
    for key, entries in sorted(history.items()):
        for entry in entries:
            if not entry or entry == _entry:
                continue
            _entry = list(entry)
            if not entry[-1].endswith(b'\n'):
                write(1, b'%s\n%s\n'%(key, join(entry)))
            else:
                write(1, b'%s\n%s'%(key, join(entry)))


def main(parser=None, prog=None, history=None):
    parser = main_parser(parser=parser, prog=prog)
    args = parser.parse_args()
    try:
        history, errors = main_process(args, history)
    except Missing1Group:
        parser.error(f'missing 1 group for regexp: {args.regexp!r}')
    main_print(history)
    return history, errors


if __name__ == '__main__':
    try:
        history, errors = main()
    except KeyboardInterrupt:
        print('\nWARNING KeyboardInterrupt', file=sys.stderr)
        sys.exit(-1)
    except BrokenPipeError:
        print('WARNING BrokenPipeError', file=sys.stderr)
        sys.exit(-2)
    else:
        if errors:
            for path, error in errors:
                print(f'ERROR Could not open: {path!r} ({error})', file=sys.stderr)
            sys.exit(len(errors))
